<?php
class Admin_Controller extends Base_Controller {

public $restful = true;
private $departments;
private $admin_roles;
private $users_roles;
private $moderator_roles;
public function __construct() {
parent::__construct();

if (Cache::has('admin_defaults')) {//check if the cache has already the ```admin_defaults``` item

$admin_defaults = Cache::get('admin_defaults');
//get the admin defaults
$this -> departments = $admin_defaults['departments'];
$this -> admin_roles = $admin_defaults['admin_roles'];
$this -> users_roles = $admin_defaults['users_roles'];
$this -> moderator_roles = $admin_defaults['moderator_roles'];

} else {//if the cache doesn't have it yet

$this -> departments = DB::table('departments') -> get();
$this -> admin_roles = DB::table('roles') -> where('department_id', '=', 1) -> get('role');
$this -> users_roles = DB::table('roles') -> where('department_id', '=', 2) -> get('role');
$this -> moderator_roles = DB::table('roles') -> where('department_id', '=', 3) -> get('role');

//cache the database results so we won't need to fetch them again for 10 minutes at least
Cache::put('admin_defaults', array('departments' => $this -> departments, 'admin_roles' => $this -> admin_roles, 'users_roles' => $this -> users_roles, 'moderator_roles' => $this -> moderator_roles), 10);
}

$this -> filter('before', 'auth');
//run the auth filter before every request of this controller

}
public function get_new_user(){

return View::make('admin.create_user',
array(
'departments'     => $this->departments,
'aroles'        => $exist = DB::table('roles')->where('department_id','=',1)->get()
));
}
public function get_home(){

return View::make('admin.home');
}
public function post_new_user(){

//get user details
$firstname  = Input::get('firstname');
$middlename = Input::get('middlename');
$lastname   = Input::get('lastname');
$department = Input::get('department');
$role       = Input::get('role');
$username   = Input::get('username');
$password   = Input::get('password');
$rules = array(
'firstname' => 'required',
'username'     => 'required',
'password'     => 'required',
'middlename' => 'required',
'department'     => 'required',
'role' => 'required',
'lastname'     => 'required'

);

$validation = Validator::make(Input::all(), $rules);

if ($validation->fails())
{

return Redirect::to('admin/new_user')
->with_errors($validation)    //pass in errors
->with_input();
}else{
$password = Hash::make($password); //hash the password

//get the department id of the department selected by the user
$selected_department = Department::where('department', '=', $department)
->first('id');

$department_id = $selected_department->id;

//get the role id of the role selected by user
$selected_role = DB::table('roles')->where('role','=',$role)->get();
//dd($selected_role);
$role_id = $selected_role[0]->id;
/* chech if  user name taken)*/
$exist = DB::table('users')->where('username','=',$username)->only('firstname');

if($exist == FALSE){
// add user normaly
}else{
return Redirect::to('admin/new_user')
->with('error', '"'.$exist .'" had already taken this username please try another one ')    //pass in errors
->with_input();
}
$user_data = array(
'firstname' => $firstname,
'middlename' => $middlename,
'lastname' => $lastname,
'department_id' => $department_id,
'role_id' => $role_id,
'username' => $username,
'hashed_password' => $password,
'status'=> 1
);

DB::table('users')->insert($user_data);
$user_id = DB::connection('mysql')->pdo->lastInsertId();

$cache_data = Input::all(); //get all user inputs

//additional data to be included in the cache
$cache_data['user_id'] = $user_id;
$cache_data['status'] = 1;
$this->add_cache_item('users', $cache_data); //add the new user to the cache

$this->log_action("create new user"); //log the action into the database

//redirect to the create user form with the awesome success message
return Redirect::to('admin/new_user')
->with('success_message', 'User Successfully Created');
}
}
private function add_cache_item($key, $data){
if(Cache::has($key)){ //check if item exists in the cache
$current_data = Cache::get($key); //store it to a variable
array_push($current_data, (object)$data); //add the data to the variable
Cache::put($key, $current_data, 10); //replace the data which is currently on the cache with the new one
}
}
function log_action($transaction){
$user_id = Session::get('current_user_id');
$user = Session::get('current_user');
$department = URI::segment(1);

//save the log into the database
DB::table('userlogs')->insert(
array(
'user_id' => $user_id,
'user' => $user,
'department' => $department,
'transaction' => $transaction
)
);
}
public function get_new_role(){

$departments = DB::table('departments')->get();
return View::make('admin.create_role')
->with('departments' , $this->departments);
}

public function post_new_role(){

//get the data inputted by the user
$department = Input::get('department');
$role = Input::get('role');
$rules = array(
'department'     => 'required',
'role' => 'required'

);

$validation = Validator::make(Input::all(), $rules);

if ($validation->fails())
{

return Redirect::to('admin/new_role')
->with_errors(Role::$validation)
->with_input();
}else{
$selected_department = DB::table('departments')
->where('department', '=', $department)
->first('id');

$department_id = $selected_department->id;
$exist = DB::table('roles')->where('role','=',$role)->only('role');

if($exist == FALSE){
// add user normaly
}else{
return Redirect::to('admin/new_role')
->with('error', '"'.$exist .'"  already exist please try another one ')    //pass in errors
->with_input();
}

DB::table('roles')->insert(
array(
'department_id' => $department_id,
'role' => $role
));

$role_id = DB::connection('mysql')->pdo->lastInsertId(); //get the last inserted primary key

//select all the transactions (things that can be done for the selected department)
$transactions = DB::table('transactions')
->where('department_id', '=', $department_id)
->get();

//loop through the results
foreach($transactions as $t){
$transaction_id = $t->id; //get the transaction id

//save to the rolestransactions table
DB::table('rolestransactions')
->insert(array('role_id' => $role_id, 'transaction_id' => $transaction_id));
}

if(Cache::has('admin_defaults')){ //check if a cache item for admin_defaults already exists
$admin_defaults = Cache::get('admin_defaults');

//add the new role into the cache
array_push($admin_defaults[$department . '_roles'], (object)array('role' => $role));
Cache::put('admin_defaults', $admin_defaults, 10);
}

$this->log_action("create new role"); //save the action to the database

//redirect to the create role form with the success message
return Redirect::to('admin/new_role')
->with('success_message', 'Role Successfully Created');
}
}

public function post_add_transactions_to_rols(){
//get the data inputted by the user


 if(Input::get('step1') == "true"){

$role = Input::get('role');


$transactions = DB::table('transactions')
->join('rolestransactions','transactions.id','=','rolestransactions.transaction_id')
->where('role_id','=',$role)->get();
$cando =array();
foreach ($transactions as $data) {
	$cando[]=$data->transaction_id;
	
}
$all = DB::table('transactions')->where('department_id','=','1')->get();

$allowedtransactions =array();
foreach ($all as  $ele) {
		if(!in_array($ele->id ,$cando)){
			$allowedtransactions[]=$ele->id;
		}
}
$where ='';
foreach ($allowedtransactions as $key ) {
	$where .= 'id ='.$key.' or ';
	
}
$where .='id =0';
//dd($where);
$rolestransactions = DB::query('SELECT * from transactions where '.$where);
//dd($rolestransactions);
return view::make('admin.role_transaction')
->with('success_message', 'please check this peroperties')
->with('transactions',$transactions)
->with('rolestransactions',$rolestransactions)
->with('role',DB::table('roles')->where('id','=',$role)->only('role'))
->with('role_id',$role)
->with('roles',false);

}

$department = Input::get('cando');

$role = Input::get('role_id');
//dd($department);
$affected = DB::table('rolestransactions')->where('role_id', '=', $role)->delete();
if($department != ''){
foreach($department as $item){
	
DB::table('rolestransactions')->insert(
array(
'role_id' => $role,
'transaction_id' => $item,
'status'=> 1
));
	
}
}
$this->log_action("update ".$role." transactions"); //save the action to the database

return Redirect::to('admin/new_role')
->with('success_message', 'transactions  Successfully updated');
}

public function get_add_transactions_to_rols(){

$departments = DB::table('roles')->where('department_id','=','1')->get();

return View::make('admin.role_transaction')
->with('roles', $departments)
->with('transactions','false');
/*
$transactions = DB::table('transactions')->where('department_id','=',$input)->get();

return View::make('admin.role_transaction')
->with('roles', $departments)
->with('transactions',$transactions);*/
}

public function post_new_transaction(){

//get the data inputted by the user
$department = Input::get('department');
$main_menu = Input::get('main_menu');
$menu_text = Input::get('menu_text');
$address = Input::get('address');
$rules = array(
'department'     => 'required',
'menu_text' => 'required',
'main_menu'=> 'required',
'address'=> 'required'
);

$validation = Validator::make(Input::all(), $rules);

if ($validation->fails())
{

return Redirect::to('admin/new_transaction')
->with_errors(Role::$validation)
->with_input();
}else{
$selected_department = DB::table('departments')
->where('department', '=', $department)
->first('id');

$department_id = $selected_department->id;
$exist = DB::table('transactions')->where('address','=',$address)->only('address');

if($exist == FALSE){
// add user normaly
}else{
return Redirect::to('admin/new_transaction')
->with('error', '"'.$exist .'"  already exist in the passes  please try another one ')    //pass in errors
->with_input();
}

DB::table('transactions')->insert(
array(
'department_id' => $department_id,
'menu_text' => $menu_text,
'main_menu'=> $main_menu,
'address'=> $address,
'status'=>1
));

$role_id = DB::connection('mysql')->pdo->lastInsertId(); //get the last inserted primary key

//select all the transactions (things that can be done for the selected department)

$this->log_action("create new transaction"); //save the action to the database

//redirect to the create role form with the success message
return Redirect::to('admin/new_transaction')
->with('success_message', 'transaction Successfully Created');
}
}

public function get_new_transaction(){

$departments = DB::table('departments')->get();
return View::make('admin.create_transaction')
->with('departments', $this->departments);
}

public function post_update_userstatus($user_id, $status){

//update user status
DB::table('users')
->where('id', '=', $user_id)
->update(array('status' => $status));

log_action("update user status");

//determine the new status
$new_status = ($status == 1) ? 0 : 1;       //the new status will be equal to 0 if status is 1, it will be equal to 1 if status is 0

echo $new_status;   //echo it out, this is the response that were getting earlier from JavaScript
}
public function get_users(){

//get the users from the database
$users = DB::table('users')
->join('departments', 'users.department_id', '=', 'departments.id')
->join('roles', 'users.role_id', '=', 'roles.id')
->get(array('users.id', 'username', 'firstname', 'lastname', 'department', 'role', 'status'));

$users_data = $users;

//render the view for viewing the list of users
//dd($users_data);
return View::make('admin.users')
->with('users', $users_data);

}
public function get_user($user_id){
$user = $this->_get_userdata($user_id);

return View::make(
'admin.user',
array(
'user' => $user,
'departments' => $this->departments,
'admin_roles' => $this->admin_roles,
'assessor_roles' => $this->assessor_roles,
'treasury_roles' => $this->treasury_roles
)
);
}
private function _get_userdata($user_id){
$user_data = DB::table('users')
->join('departments', 'users.department_id', '=', 'departments.id')
->join('roles', 'users.role_id', '=', 'roles.id')
->where('users.id', '=', $user_id)
->first(array('users.id', 'username', 'firstname', 'middlename', 'lastname', 'department', 'role', 'status'));

return $user_data;
}
public function post_user($user_id){
$firstname = Input::get('firstname');
$middlename = Input::get('middlename');
$lastname = Input::get('lastname');
$department = Input::get('department');
$role = Input::get('role');
$username = Input::get('username');
$password = Input::get('password');

//bend the rules for updating user data
User::$rules['username'] = 'required|unique:users,username,' .$user_id . ',user_id'; //username is still unique but we need to exclude the user that were currently updating by specifying the user id

User::$rules['password'] = ''; //by default password is required now it isn't

if(!User::is_valid()){

return Redirect::to('admin/user/' . $user_id)
->with_errors(User::$validation)
->with_input();
}else{
$selected_department = DB::table('departments')
->where('department', '=', $department)
->first('id');

$department_id = $selected_department->id;

$selected_role = DB::table('roles')
->where('role', '=', $role)->first('id');

$role_id = $selected_role->id;

if(!empty($password)){ //data if password is updated
$password = Hash::make($password);
$user_data = array(
'firstname' => $firstname,
'middlename' => $middlename,
'lastname' => $lastname,
'department_id' => $department_id,
'role_id' => $role_id,
'username' => $username,
'hashed_password' => $password
);
}else{ //data if password is not update
$user_data = array(
'firstname' => $firstname,
'middlename' => $middlename,
'lastname' => $lastname,
'department_id' => $department_id,
'role_id' => $role_id,
'username' => $username
);
}

//update user details
DB::table('users')
->where('id', '=', $user_id)
->update($user_data);

$this->log_action("update user");

return Redirect::to('admin/user/' . $user_id)
->with('success_message', 'User Successfully Updated');
}
}
public function get_logs($user_id = ''){
$scope = '';
if(!empty($user_id)){
$userlogs = DB::query("
SELECT * FROM userlogs
WHERE user_id = '$user_id'
ORDER BY dateandtime DESC
");
if(!empty($userlogs)){
$scope = $userlogs[0]->user;
}
}else{
$userlogs = DB::query('
SELECT * FROM userlogs
WHERE DATE(dateandtime) = CURRENT_DATE
ORDER BY dateandtime DESC
');
$scope = 'All Users';
}

return View::make('admin.userlogs', array('userlogs' => $userlogs, 'scope' => $scope));
}
public function get_roles($role_id){
$roles = DB::table('roles')->get();
$transactions = DB::table('transactions')
->join(
'rolestransactions',
'transactions.id', '=', 'rolestransactions.transaction_id'
)
->where('role_id', '=', $role_id)
->get(array(
'rolestransactions.id', 'role_id',
'department_id', 'menu_text', 'status'
)
);

return View::make(
'admin.transactions',
array('roles' => $roles, 'transactions' => $transactions, 'role_id' => $role_id)
);
}
public function post_update_transactionstatus($roletransaction_id, $status){
DB::table('roletransactions')
->where('id', '=', $roletransaction_id)
->update(array('status' => $status));

log_action("update transaction status");
}
}
?>