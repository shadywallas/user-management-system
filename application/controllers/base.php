<?php

class Base_Controller extends Controller {

	/**
	 * Catch-all method for requests that can't be matched.
	 *
	 * @param  string    $method
	 * @param  array     $parameters
	 * @return Response
	 */
	  public function __construct(){
        //styles
        Asset::add('main_style', 'css/main.css');
        Asset::add('jquery', 'js/jquery.js');
        Asset::add('foundation_style', 'libs/foundation/stylesheets/foundation.min.css');
        
        
        //scripts
        Asset::add('foundation_script', 'libs/js/foundation.min.js');
        Asset::add('jqueryui_script', 'libs/jquery-ui/jquery-1.9.1.js');
        Asset::add('main_script', 'js/main.js');

        parent::__construct();
    }
	public function __call($method, $parameters)
	{
		return Response::error('404');
	}

}