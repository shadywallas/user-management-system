<?php

class Login_Controller extends Base_Controller {

	public $restful = true;
	public function get_index() {
		if (Auth::check()) 
			{//check if the user is already logged in
			return Redirect::to('/');
			}
		return View::make('login');
		//renders the login.blade.php located in the root of the view folder
	}

	public function post_index() {
		$username = Input::get('username');
		$password = Input::get('password');
		$user_details = Input::all();
		//Input::all() gets all the data inputted in the form
		$rules = array('username' => 'required', 'password' => 'required');
		$validation = Validator::make($user_details, $rules);
		if ($validation -> fails()) {

			return Redirect::to('login') -> with_errors($validation);
		}
		if (Auth::attempt($user_details)) {//attempt to login the user
			if (Auth::check()) 
			{//check if the user is already logged in
							
				
				$user_id = Auth::user() -> id;
				
				//get user data
				$user = DB::table('users') -> where('id', '=', $user_id) -> first(array('department_id', 'role_id', 'status'));

				$status = $user -> status;

				if ($status == 1) {//check if user account is enabled
					$department_id = $user -> department_id;
					$role_id = $user -> role_id;

					//save user details into session
					Session::put('department_id', $department_id);
					Session::put('role_id', $role_id);
					Session::put('current_user', $username);
					Session::put('current_user_id', $user_id);

					//the departments available in the system
					$departments = array("admin", "users", "moderators");

					//redirect user to his departments homepage
					return Redirect::to($departments[$department_id - 1] . '/home');

				} else {//if user account is disabled
				Auth::logout();
					return Redirect::to('login') -> with('error', 'Disabled users cannot login');
				}
			}

		}else {
	return Redirect::to('login')->with('error', 'Wrong user or pass ') ;
}
	}

}
