<?php

class Add_Foreignkeys_To_Users_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//
						Schema::table('users', function($table) {

			$table -> foreign('role_id') -> references('id') -> on('roles');

		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}