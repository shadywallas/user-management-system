<?php

class Insert_Into_Users_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		DB::table('users')->insert(
  array(
      'firstname' => 'Hibari',
      'middlename' => 'Neo',
      'lastname' => 'Kyoya',
      'department_id' => 1,
      'role_id' => 1,
      'username' => 'hkyoya',
      'hashed_password' => Hash::make('madoffun'),
      'status' => 1
  )
);
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}