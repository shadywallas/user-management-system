<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/
Route::Controller(Controller::detect()); //create a route for every controller so that its accessible

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});
View::composer(
//specify the views where the data will be attached
  array(
    'admin.home','main',
   'admin.creat_role','admin.create_user', 
    'admin.user', 
    'admin.userlogs', 'admin.transactions'
  ), 
    function($view){

    $role_id = Session::get('role_id'); //load role id of current user from session

    if(Session::get('navigation')){
      $nav = Session::get('navigation');

    }else{
      $transactions = DB::table('rolestransactions')
        ->join('transactions','rolestransactions.transaction_id', '=', 'transactions.id')
        ->where('role_id', '=', $role_id)
        ->where('status', '=', 1)
        ->get(array(
            'menu_text', 'main_menu', 'address', 'status'
          )
        );

      $nav = array();
      foreach($transactions as $t){

        $nav[$t->main_menu][$t->menu_text] = $t->address; //fill in the array with the results from the database
      }

      //save it into the session to make it accessible later 
      //on pages which needs this information
      Session::put('navigation', $nav); 
    }

    //attach the data that was fetched from the database into the navigation view
    //first parameter is the name that you want to give the view
    //second parameter is the actual name of the view. In this case the name is navigation.blade.php
    //third is the data that you wish to pass in to the navigation view
        $view->nest('navigation', 'navigation', array('navigation' => $nav));
    }
);
Route::filter('check_roles', function()
{
    $current_url = URI::current();  //get current url excluding the domain.
    $current_page = URI::segment(2); //get current page which is stored in the second uri segment. Just a refresher: //the first uri segment is the controller, the second is the method, 
        //third and up are the parameters that you wish to pass in

    $access = 0;
    $role_id = Session::get('role_id'); //load role id of current user from session
     $transactions = DB::table('rolestransactions')
        ->join('transactions','rolestransactions.transaction_id', '=', 'transactions.id')
        ->where('role_id', '=', $role_id)
        ->where('status', '=', 1)
        ->get(array(
            'menu_text', 'main_menu', 'address', 'status'
          )
        );

      $nav = array();
      foreach($transactions as $t){

        $nav[$t->main_menu][$t->menu_text] = $t->address; //fill in the array with the results from the database
      }
    
   // dd($nav);
    //excluded pages are the pages we don't want to execute this filter 
    //since they should always be accessible for a logged in user
    $excluded_pages = array('login', 'admin/home');

    if(!in_array($current_url, $excluded_pages)){//if current page is not an excluded pages
      foreach($nav as $addresses){
        foreach($addresses as $address){
          if(strpos($address, "/")){
            $full = explode("/", $address);
            $page = $full[1];
            if($current_page == $page){
              $access = 1;
              return; 
            }
          }
        }
      }

      if($access == 0){ //if user doesn't have access to the page that he's trying to access
        $departments = array('admin', 'galary', 'users'); 
        $department_id = Session::get('department_id'); //get department id of the current user

        //redirect the user to the homepage of that 
        //department along with some error message that he cannot access that page
        return Redirect::to($departments[$department_id - 1].'/home') 
          ->with('error', 'You don\'t have permission to access the following page: ' . $current_url);
      }
    }

});
Route::get('logout', function(){
  Auth::logout(); //logout the current user
  Session::flush(); //delete the session
  return Redirect::to('login'); //redirect to login page
});
Route::filter('pattern: admin/*', 'check_roles');

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});