@layout('main')

@section('content')
    <div class="row">
        <div class="twelve columns">
            <h5>Create transaction</h5>
        </div>
    </div>
    <div class="row">
        @render('errors')
        @render('success')
        <form method="post">
            <div class="row">
                    <div class="two columns">
                  <label for="department" class="right inline">Department</label>
                </div>
                <div class="ten columns">
                  <input type="text" name="department" id="department" class="five" list="departments"/>
                  <datalist id="departments">
                  @foreach($departments as $dept)
                    <option value="{{ $dept->department }}">{{ $dept->department }}</option>
                  @endforeach
                  </datalist>
                </div>

                <div class="two columns">
                  <label for="role" class="right inline">main_menu </label>
                </div>
                <div class="ten columns">
                  <input type="text" name="main_menu" id="main_menu" class="five" />
                </div>
         <div class="two columns">
                  <label for="role" class="right inline">menu_text </label>
                </div>
                <div class="ten columns">
                  <input type="text" name="menu_text" id="menu_text" class="five" />
                </div>
         <div class="two columns">
                  <label for="role" class="right inline">address </label>
                </div>
                <div class="ten columns">
                  <input type="text" name="address" id="address" class="five" />
                </div>

            </div>

          <div class="row">
            <div class="four columns">

            </div>
            <div class="eight columns">
              <button class="success medium button" href="#">Create transaction</button>
            </div>
          </div>
        </form>

    </div>
@endsection