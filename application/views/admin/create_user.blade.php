@layout('main')

@section('content')
<div class="row">
	<div class="twelve columns">
		<h5>Create user</h5>
	</div>
</div>
<div class="row">
	@render('errors')
	@render('success')
	<form method="post" name="user">
		<div class="row">
			<div class="two columns">
				<label for="role" class="right inline">F name </label>
			</div>
			<div class="ten columns">
				<input type="text" name="firstname" id="firstname" class="five" />
			</div>
			<div class="two columns">
				<label for="role" class="right inline"> m name  </label>
			</div>
			<div class="ten columns">
				<input type="text" name="middlename" id="middlename" class="five" />
			</div>
			<div class="two columns">
				<label for="role" class="right inline">L name </label>
			</div>
			<div class="ten columns">
				<input type="text" name="lastname" id="lastname" class="five" />
			</div>
			<div class="two columns">
				<label for="department" class="right inline">Department</label>
			</div>
			<div class="ten columns">
				<input type="text" name="department" id="department" class="five" list="departments"/>
				<datalist id="departments">
					@foreach($departments as $dept)
					<option value="{{ $dept->department }}">{{ $dept->department }}</option>
					@endforeach
				</datalist>
			</div>

			
<div class="two columns">
				<label for="department" class="right inline">role</label>
			</div>
			<div class="ten columns">
				<select name="role" id="role">
					@foreach($aroles as $role)
					<option value="{{ $role->role }}">{{ $role->role }}</option>
					@endforeach
				</select>
			</div>

			
			<div class="two columns">
				<label for="role" class="right inline">username </label>
			</div>
			<div class="ten columns">
				<input type="text" name="username" id="username" class="five" />
			</div>
			<div class="two columns">
				<label for="role" class="right inline">password </label>
			</div>
			<div class="ten columns">
				<input type="text" name="password" id="password" class="five" />
			</div>
		</div>

		<div class="row">
			<div class="four columns">

			</div>
			<div class="eight columns">
				<button class="success medium button" href="#">
					Create user
				</button>
			</div>
		</div>
	</form>

</div>
@endsection