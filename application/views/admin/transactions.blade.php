@layout('main')

@section('content')
<div class="row">
	<div class="twelve columns">
		<h5>Roles and Transactions</h5>
	</div>
</div>

<div class="row">
	<div class="two columns">
		<label for="role" class="right inline">Role</label>
	</div>
	<div class="nine columns">
		<select name="" id="role" class="seven">
			@foreach($roles as $r)
			<option
			data-deptid="{{ $r->department_id }}"

			data-roleid="{{ $r->id }}"

			value="{{ $r->role }}"
			@if($role_id == $r->id)
				{{ "selected" }}
				@endif
				>
				{{ $r->role }} </option>
			@endforeach
		</select>
		<a href="" id="view_transactions" class="success medium button two">View</a>
	</div>
</div>

<div id="transactions" class="row transactions">
	<div class="twelve columns">
		<ul>
			@foreach($transactions as $t)
			<li>
				<?php $status = "";
				if ($t -> status == 1) {
					$status = "checked";
				}
				?>
				<input type="checkbox" data-id="{{ $t->id }}" {{ $status }}>
				{{ $t->menu_text }}
			</li>
			@endforeach
		</ul>
	</div>
</div>
@endsection

@section('script')
<script>
	$("#role").change(function() {//if the role is changed
		var url = window.location;
		var selected_role = $("#role").find(":selected");
		//its a good practice to cache a selector if you're going to use it more than once

		var department_id = selected_role.data("deptid");
		var role_id = selected_role.data("roleid");

		//change the url of the view button(well not actually a button, its a link) based on the role id
		$("#view_transactions").attr("href", url.origin + "/admin/roles/" + role_id);

	});

	$("input[data-id]").click(function() {
		var roletransaction_id = $(this).data("id");
		var status = $(this).attr("checked") ? 1 : 0;
		//if the box is check the status is 1 if not then its 0

		var url = window.location;
		//get all the information about the current url

		//update transaction status via ajax
		$.post(url.origin + "/admin/update_transactionstatus/" + roletransaction_id + "/" + status, function(response) {

		});
	}); 
</script>
@endsection