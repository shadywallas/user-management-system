@layout('main')

@section('content')
<div class="row">
  <div class="twelve columns">
    <h5>Update User</h5>
  </div>
</div>
<div class="row">
  @render('errors')
  @render('success')
  <form method="post">    
    <div class="row">    
      <div class="two columns">
        <label for="firstname" class="right inline">First Name </label>
      </div>
      <div class="ten columns">
        <input type="text" name="firstname" id="firstname" class="five" value="{{ $user->firstname }}"/>
      </div>

      <div class="two columns">
        <label for="middlename" class="right inline">Middle Name </label>
      </div>
      <div class="ten columns">
        <input type="text" name="middlename" id="middlename" class="five" value="{{ $user->middlename }}"/>
      </div>

      <div class="two columns">
        <label for="lastname" class="right inline">Last Name</label>
      </div>
      <div class="ten columns">
        <input type="text" name="lastname" id="lastname" class="five" value="{{ $user->lastname }}"/>
      </div>

      <div class="two columns">
        <label for="username" class="right inline">Username</label>
      </div>
      <div class="ten columns">
        <input type="text" name="username" id="username" class="five" value="{{ $user->username }}"/>
      </div>

      <div class="two columns">
        <label for="password" class="right inline">Password</label>
      </div>
      <div class="ten columns">
        <input type="password" name="password" id="password" class="five"/>
      </div>   

      <div class="two columns">
        <label for="department" class="right inline">Department</label>
      </div>
      <div class="ten columns">
        <input type="text" name="department" id="department" class="five" value="{{ $user->department }}" list="departments"/>

        <datalist id="departments">
          @foreach($departments as $dept)
          <option value="{{ $dept->department }}">{{ $dept->department }}</option>
          @endforeach
        </datalist>
      </div>  

      <div class="two columns">
        <label for="role" class="right inline">Role</label>
      </div>
      <div class="ten columns">
        <input type="text" name="role" id="role" class="five" value="{{ $user->role }}" list=""/>
        <datalist id="admin">
          @foreach($admin_roles as $a)
            <option value="{{ $a->role }}">{{ $a->role }}</option>
          @endforeach
        </datalist>

        <datalist id="assessor">
          @foreach($assessor_roles as $b)
            <option value="{{ $b->role }}">{{ $b->role }}</option>
          @endforeach
        </datalist>

        <datalist id="treasury">
          @foreach($treasury_roles as $c)
            <option value="{{ $c->role }}">{{ $c->role }}</option>
          @endforeach       
        </datalist>
      </div>  
    </div>

    <div class="row">
      <div class="four columns">

      </div>
      <div class="eight columns">
        <button class="success medium button" href="#">Update User</button>
      </div>
    </div>
  </form>
</div>
@endsection