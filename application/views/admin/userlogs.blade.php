@layout('main')

@section('content')
<div class="row">
  <div class="twelve columns">
    <h5>User Logs - {{ $scope }}</h5>
  </div>
</div>
<div class="row">
    <div class="twelve columns">
        <table class="twelve dtable">
          <thead>
            <tr>
              <th>Username</th>
              <th>Department</th>
              <th>Activity</th>
              <th>Timestamp</th>
            </tr>
          </thead>
          <tbody>
            @foreach($userlogs as $logs)
            <tr>
                <td>{{ $logs->user }}</td>
                <td>{{ $logs->department }}</td>
                <td>{{ $logs->transaction }}</td>
                <td>{{ date("M d, Y @ g:i A", strtotime($logs->dateandtime)) }}</td>
            </tr>
            @endforeach
          </tbody>
      </table>
    </div>
</div>
@endsection