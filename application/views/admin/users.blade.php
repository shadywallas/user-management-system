@layout('main')

@section('content')
<div class="row">
    <div class="twelve columns">
        <h5>Users</h5>
    </div>
</div>

<div class="row">
    <table class="twelve dtable">
      <thead>
        <tr>
          <th>Username</th>
          <th>Name</th>
          <th>Department</th>
          <th>Role</th>
          <th>Logs</th>
          <th>Status</th>
          <th>Update</th>
        </tr>
      </thead>
      <tbody>
  
        @foreach($users as $user)
        <?php
        $icons = array("foundicon-lock", "foundicon-unlock");
        $actions = array(1, 0);
        ?>
  
        <tr>
            <td>{{ $user->username }}</td>
            <td>{{ $user->firstname . " " . $user->lastname }}</td>
            <td>{{ $user->department }}</td>
            <td>{{ $user->role }}</td>
            
            <td class="pointer">
            	<a href="{{ URL::to('admin/logs/' . $user->id) }}">
                    <i class="icons foundicon-address-book" data-url="{{ URL::to('admin/logs/' . $user->id) }}"></i>
            </a>
            </td>
            <td class="pointer">
                <a href="{{ URL::to('admin/logs/' . $user->id) }}">
                <i class="icons {{ $icons[$user->status] }}" data-action="{{ $actions[$user->status] }}" data-uid="{{ $user->id }}"></i>
            </a>
            </td>
            
            <td class="pointer">
                <a href="{{ URL::to('admin/user/' . $user->id) }}">
                <i class="icons foundicon-refresh" data-url="{{ URL::to('admin/user/' . $user->id) }}"></i>
            </a>
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection

@section('script')
<script>
$("td.pointer").live("click", function(){
    //get the data-url value from the <i> tag
    //it feels weird to put data attributes to a table definition so 
    //we have to do some selector-fu to get to the data that we want
    var url = $(this).children().data('url'); 

    if(url){ //just making sure that url is storing a truthy value

        //cheap trick for changing page location
        //I wouldn't actually do it this way
        //if the anchor tag <a> works when its wrapping an <i> tag
        window.location = url; 

    }
});

$("td.pointer").hover(function(){
    //on hover: make the user see that he's actually hovering on something by changing the color
    $(this).children().css("color", "#0CAEE3");
}, function(){
    //on mouse out: change back to the original color
    $(this).children().css("color", "#333");
});

$("td.pointer").click(function(){
    var url = window.location; //get all the information of the current page (Eg. host, hostname, port, etc.)

    //caching the information about the child of the clicked element so we won't have to select it again later 
    var child = $(this).children();  

    var user_id = child.data("uid"); //accessing the value of data-uid attribute using the cache item(child) as the base
    var status = child.data("action"); //accessing the value of data-uid attribute using the cache item(child) as the base

    //update user status using ajax
    $.post(
        url.origin + "/admin/update_userstatus/" + user_id + "/" + status,
        function(response){ //the response is either 1 or 0, if the admin disabled the user the response would be the opposite which is 1. If the admin enabled the user the response would be 0

            var icons = ["foundicon-unlock", "foundicon-lock"]; 
            child.removeClass(); 
            child.addClass("icons " + icons[response]); //change the icon based on the response
            child.data("action", response); //change the data-action attribute based on response
        }
    );
});
</script>
@endsection
