@layout('public')

@section('content')
<div class="row">
  <div class="twelve columns">
    <h5>Login</h5>
  </div>
</div>
<div id="login_container" class="row">
  @render('errors')
  <form method="post">
    <div class="row">    
      <div class="two columns">
        <label class="right inline">Username</label>
      </div>
      <div class="ten columns">
        <input type="text" name="username" class="five" />
      </div>

      <div class="two columns">
        <label class="right inline">Password</label>
      </div>
      <div class="ten columns">
        <input type="password" name="password" class="five" />
      </div>
    </div>

    <div class="row">
      <div class="five columns">

      </div>
      <div class="seven columns">
        <button class="success medium button" href="#">Login</button>
      </div>
    </div>

  </form>
</div>
@endsection  