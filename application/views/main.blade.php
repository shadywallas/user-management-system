<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>UMS</title>
        {{ Asset::styles() }}
    </head>
    <body>

        <div class="row">
          <div class="twelve columns">
            <nav class="top-bar">
              <ul>
                <!-- Title Area -->
                <li class="name">
                  <img src="{{ URL::to_asset('img/umslogo.png') }}" alt="ums_logo">
                </li>
                <li class="name">
                  <h1>
                    <a href="#">
                      UMS
                    </a>
                  </h1>
                </li>
                <li class="toggle-topbar"><a href="#"></a></li>
              </ul>

              <section>
                <!-- Left Nav Section -->
                {{ $navigation }}
                <ul class="right">
                  <li class="divider"></li>
                  <li class="name">
                    <a href="">{{ Session::get('current_user') }}</a> 
                  </li>
                  <li class="divider"></li>
                  <li class="name">
                    <a href="{{ URL::to('logout') }}">Logout</a>
                  </li>
                </ul>

              </section>
            </nav>
          </div>
        </div>
        <div class="container">
            @yield('content')
            <hr>
            <footer>
                <p>&copy; UMS 2012</p>
            </footer>
        </div> <!-- /container -->
        {{ Asset::scripts() }}
        @yield('script')
    </body>
</html>