<ul class="left">
  <li class="divider"></li>
    @foreach($navigation as $key => $nav)
      <li class="has-dropdown">
        <a href="#" class="active">{{ $key }}</a>
        <ul class="dropdown">
          @foreach($nav as $subnav => $link)
          <li>
            <a href=" {{ URL::to($link) }} ">{{ $subnav }}</a>
          </li>
          @endforeach
        </ul>
      </li>
    @endforeach
</ul>