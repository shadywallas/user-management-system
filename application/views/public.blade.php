<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>UMS</title>
        {{ Asset::styles() }}
    </head>
    <body>
            <div class="row">
        <div class="twelve columns">
          <nav class="top-bar">
            <ul>
              <!-- Title Area -->
              <li class="name">
                <img src="{{ URL::to_asset('img/umslogo.png') }}" alt="ums_logo">
              </li>
              <li class="name">
                <h1>
                  <a href="#">
                    UMS
                  </a>
                </h1>
              </li>
              <li class="toggle-topbar"><a href="#"></a></li>
            </ul>
          </nav>
        </div>
      </div>        
        <div class="container">
            @yield('content')
            <footer>
                 <p>&copy; UMS 2012</p>
            </footer>
        </div> <!-- /container -->
        {{ Asset::scripts() }}
    </body>
</html>  