@layout('main')

@section('content')
<div class="row">
  <div class="twelve columns">
    <h5>Create User</h5>
  </div>
</div>
<div class="row">
  @render('errors')
  @render('success')
  <form method="post">
    <div class="row">    
      <div class="two columns">
        <label for="firstname" class="right inline">First Name </label>
      </div>
      <div class="ten columns">
        <input type="text" name="firstname" id="firstname" class="five" value="{{ Input::old('firstname') }}"/>
      </div>

      <div class="two columns">
        <label for="middlename" class="right inline">Middle Name </label>
      </div>
      <div class="ten columns">
        <input type="text" name="middlename" id="middlename" class="five" value="{{ Input::old('middlename') }}"/>
      </div>

      <div class="two columns">
        <label for="lastname" class="right inline">Last Name</label>
      </div>
      <div class="ten columns">
        <input type="text" name="lastname" id="lastname" class="five" value="{{ Input::old('lastname') }}"/>
      </div>

      <div class="two columns">
        <label for="username" class="right inline">Username</label>
      </div>
      <div class="ten columns">
        <input type="text" name="username" id="username" class="five" value="{{ Input::old('username') }}"/>
      </div>

      <div class="two columns">
        <label for="password" class="right inline">Password</label>
      </div>
      <div class="ten columns">
        <input type="password" name="password" id="password" class="five" />
      </div>   

      <div class="two columns">
        <label for="department" class="right inline">Department</label>
      </div>
      <div class="ten columns">
        <input type="text" name="department" id="department" class="five" list="departments" value="{{ Input::old('department') }}"/>
        <datalist id="departments">
          @foreach($departments as $dept)
          <option value="{{ $dept->department }}">{{ $dept->department }}</option>
          @endforeach
        </datalist>
      </div>  

      <div class="two columns">
        <label for="role" class="right inline">Role</label>
      </div>
      <div class="ten columns">
        <input type="text" name="role" id="role" class="five" value="{{ Input::old('role') }}" list=""/>
        <datalist id="it">
          @foreach($it_roles as $a)
            <option value="{{ $a->role }}">{{ $a->role }}</option>
          @endforeach
        </datalist>

        <datalist id="defense">
          @foreach($defense_roles as $b)
            <option value="{{ $b->role }}">{{ $b->role }}</option>
          @endforeach
        </datalist>

        <datalist id="marketing">
          @foreach($marketing_roles as $c)
            <option value="{{ $c->role }}">{{ $c->role }}</option>
          @endforeach       
        </datalist>
      </div>  
    </div>

    <div class="row">
      <div class="four columns">

      </div>
      <div class="eight columns">
        <button class="success medium button" href="#">Create User</button>
      </div>
    </div>
  </form>

</div>
@endsection